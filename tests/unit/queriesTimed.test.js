const main = require('./main.js');
const Stopwatch = require("node-stopwatch").Stopwatch;

const fileToRead =  process.argv[2];
databases = main(fileToRead);
// id_db = databases["id_db"];
// statuses_db = databases["statuses_db"];
 
const Queries = require("../../queries.js");
queries = new Queries(databases);

// Search for the last queries in the database because that should take the longest
asset_normal = {
"id": "a3954db4-7211-4005-837a-78aa405457eb",
"assetId": "08b86fa4-b2be-4005-93e9-35d08b4c17a7",
"statusType": "NORMAL",
"createdAt": "2018-08-30T10:20:42Z"};

asset_warning = {
"id": "bea124fd-b7ed-44bb-a370-76c5ae161f27",
"assetId": "4f436a0d-903b-46bd-8e45-46c1dd3dc631",
"statusType": "WARNING",
"createdAt": "2018-08-29T06:03:56Z"};

describe("database queries performance tests", () => {        

    // Should create a large test database, just search for the very last guy in that database
    // Make a test file
    time_to_complete = 10;
    iterations = 1;
    it("should complete " + iterations + " runs of getAssetStatusesByAssetIDs in less than " + time_to_complete + " milliseconds", () => {
        var stopwatch = Stopwatch.create();
        stopwatch.start();
               
        for(var i = 0; i < 1; i++){
            latest_statuses = queries.getAssetStatusesByAssetIDs([asset_normal], log_to_console = false, print_asset_id = true);
        }

        // Having trouble printing out the time, seems to be evaluating alright though
        stopwatch.stop();        
        console.log(stopwatch.elapsedMilliseconds * 1000);
        //console.log("completed getAssetStatusesByAssetIDs in " + stopwatch.elapsedMilliseconds + " milliseconds");
        expect(stopwatch.elapsedMilliseconds).toBeLessThan(time_to_complete);
    });

    // it("should complete 100 runs of getAssetIdsByStatusType in less than " + time_to_complete + " milliseconds", () => {        
    //     var stopwatch = Stopwatch.create();
    //     stopwatch.start();
               
    //     for(var i = 0; i < 100; i++){
    //         all_assets_with_given_status = queries.getAssetIdsByStatusType("NORMAL", log_to_console = false);
    //     }

    //     stopwatch.stop();
    //     console.log("completed getAssetIdsByStatusType in " + stopwatch.elapsedMilliseconds + "milliseconds");
    //     expect(stopwatch.elapsedMilliseconds).toBeLessThan(time_to_complete);        
    // });
});