// Class containing list of queries that can be done on the database
class Queries {

    constructor(databases) {
        
        this.ids_db = databases["ids_db"];
        this.statuses_db = databases["statuses_db"];        
    }
                
        // returns array of asset statuses given array of assetIds
        // input_ids: Array of asset_status["assetIds"]
        getAssetStatusesByAssetIDs(input_ids){
            
            let latest_asset_statuses = [];
            
            input_ids.forEach(input_id => {

                // Want to return null if the asset isn't found
                let current_asset_status = null;

                // loop through the ids_db and find the index where the assetIds property matches                  
                let index = this.ids_db.findIndex(asset_in_db => asset_in_db["assetId"] === input_id);

                // Index will be negative 1 if it is not found
                if (index  !== -1){
                    current_asset_status = this.ids_db[index];
                }

                // If the asset is not found
                else {
                    current_asset_status =   {
                        "id": "doesn't exist",
                        "assetId": input_id,
                        "statusType": null,
                        "createdAt": "doesn't exist"
                      }
                }

                latest_asset_statuses.push(current_asset_status);                 
            });

            return latest_asset_statuses;            
        }    

    // returns array of asset ids given the status type. returns null if no asset_statuses of the given type exist
    // status type : The asset_status["statusType"] property. Either "NORMAL", "WARNING", or "ERROR"
    getAssetIdsByStatusType(status_type){        

        // should include some error catching here if status_type is not specified right since there is
        // only a finite amount of status types as opposed to asset ids which need to do some regular expression stuff
        
        return this.statuses_db[status_type];
    }
}
module.exports = Queries;