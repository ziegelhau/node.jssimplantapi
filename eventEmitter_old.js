// Modified from https://www.datchley.name/es6-eventemitter/
// Unclear how the code is licensed
// Should likely change to just extend the built in class EventEmitter (const EventEmitter = require('events');)
let isFunction = function(obj) {
    return typeof obj == 'function' || false;
};

class EventEmitter {
  constructor() {
    this.listeners = new Map();
  }
  addListener(label, callback) {
    this.listeners.has(label) || this.listeners.set(label, []);
    this.listeners.get(label).push(callback);
  }

  removeListener(label, callback) {
      let listeners = this.listeners.get(label),
          index;
      
      if (listeners && listeners.length) {
          index = listeners.reduce((i, listener, index) => {
              return (isFunction(listener) && listener === callback) ?
                  i = index :
                  i;
          }, -1);
          
          if (index > -1) {
              listeners.splice(index, 1);
              this.listeners.set(label, listeners);
              return true;
          }
      }
      return false;
  }
  emit(label, ...args) {
      let listeners = this.listeners.get(label);
      
      if (listeners && listeners.length) {
          listeners.forEach((listener) => {
              listener(...args); 
          });
          return true;
      }
      return false;
  }
}

class Observer {
  constructor(id, subject, subscriptions) {
    this.id = id;
    this.subject = subject;
    subscriptions.forEach(subscription => {
        this.subject.addListener(subscription, (data) => this.handle(data));
    });
  }
  handle(message){
    console.log(message);
  }
}

module.exports = {EventEmitter, Observer};